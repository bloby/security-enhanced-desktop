# Dockerized Framework for a Security Enhanced Desktop

## Getting started

### Your OS: Debian GNU/Linux

To make all the rest easier, let's start with a common basis:

You will install a fresh computer using Debian GNU/Linux in its latest version. You'll choose a minimal graphical installation. This computer is fully "new" without any data on it.

Then you shall add Docker:

```bash
wget -O - https://0xacab.org/bloby/security-enhanced-desktop/-/raw/no-masters/bin/docker-install.sh | bash
```

You have been added to the ```docker``` group. You need to logout and then login again to make this taken into account.

## First steps

### Tor Network SOCKS proxy

First of all, install Tor utilities on your workstation. For Debian/Ubuntu, please follow the instructions for installing a proxy : https://support.torproject.org/apt/tor-deb-repo/

Then, build your Tor Proxy within your first docker container and make it autostart when you log-in:

```bash
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/docker-run/01-tor -O ~/dockers/run/01-tor
chmod +x ~/dockers/run/01-tor
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/launchers/01-tor.desktop -O - | sed "s#~#$HOME#" > ~/.local/share/applications/01-tor.desktop
ln -s ~/.local/share/applications/01-tor.desktop ~/.config/autostart/01-tor.desktop
```

Now you shall logout and login again, before continuing.

### Protecting your TCP/IP stack - a firewall

To ensure a level 2, 3 and 4 (OSI Layers) protection, it is really important to set up a firewall on your workstation. Here we would recommand using Shorewall (IPv4) and Shorewall6 (IPv6). Let's script this to accelerate the process (it works in most contexts):

```bash
torify wget "https://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/install-shorewall.sh" -O - | sed "s#\$HOME#$HOME#" | sudo bash
```

### Securing your home directory - ecryptfs

Let's continue immediatly, if it is not done yet (don't do it twice...), with the encryption of your ```$HOME```:

#### Check if your files are already encrypted with ecryptfs

```bash
$ torify wget "https://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/ecryptfs-check.sh" -O - | bash
```

If nothing says you should stop here, you may continue.

#### Prepare stuff for encryption

```bash
sudo useradd -G sudo -s /bin/bash -m temp
sudo passwd temp
torify wget https://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/encmydir.sh -O - | sed "s/<USER>/$USER/" > ~temp/
sudo chmod +x ~temp/encmydir.sh
```

Here you will enter a password for a temporary account, easy for you to remember (you will use it on your own, but only once).

#### Encrypt!

Then you will need to logout your session and open a Terminal. Let's read these few lines before doing it so you'll be able to know what to do next, without having this webpage opened (or open it in another terminal in parallel).

- After logout, press ```<CTRL>+<ALT>+<F3>``` keys at the same time
- As a login enter ```temp``` and press ```<ENTER>```
- Enter your password (it will not echo any char, it is normal, to preserv your privacy) and press ```<ENTER>```
- Then just write ```./encmydir.sh``` and press ```<ENTER>```
- It will do the job...
- Then press ```<CTRL>+<ALT>+<F1>``` to login again in your graphical environment

This has encrypted your ```$HOME``` and link it to your session opener. When logged in again, you'll have to accept once a strange window saying "execute this now". Its job is to finish the encryption process. When a terminal pops, enter your password, optionnally record the code given in your Keepass...

You are now working in an encrypted directory, and thinks can follow on.

#### Clear stuff

Do not forget to clean things before anything:

```bash
sudo userdel -r # removes the temporary account used to encrypt your home directory
rm -rf /home/$USER.???*
```

Now you're good to continue.

### Prepare your deck to our new Dockerized features

This will create the icon framework in your $HOME directory:

```bash
for elt in /usr/share/icons/hicolor/*/apps/; do echo $elt; mkdir -p $(echo $elt | sed "s#/usr#$HOME/.local#"); done
```

## Install dockerized web browsers and an email client

### Tor Browser

```bash
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/docker-run/02-tor-browser -O ~/dockers/run/02-tor-browser
chmod +x ~/dockers/run/02-tor-browser
```

TODO desktop launcher

### Mozilla Firefox

Let's install Firefox in a docker container, add it to your applications, and remove built-in ```firefox-esr```:

```bash
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/docker-run/03-firefox -O ~/dockers/run/03-firefox
chmod +x ~/dockers/run/03-firefox

# first launch
~/dockers/run/03-firefox
sleep 10

# launcher
docker cp firefox:/usr/share/applications/firefox.desktop ~/.local/share/applications/03-firefox.desktop.wip
sed -i "s#Exec=firefox#Exec=$HOME/dockers/run/03-firefox#" ~/.local/share/applications/03-firefox.desktop.wip
sed -i 's#\(Name.*=.*\)Firefox#\1Firefox-in-Docker#' ~/.local/share/applications/03-firefox.desktop.wip
grep -v 'Name\[' ~/.local/share/applications/03-firefox.desktop.wip > ~/.local/share/applications/03-firefox.desktop
rm ~/.local/share/applications/03-firefox.desktop.wip
sed "s#Exec=$HOME/dockers/run/03-firefox#Exec=$HOME/dockers/run/03-firefox noupdate#" ~/.local/share/applications/03-firefox.desktop > ~/.local/share/applications/03-firefox-noupdate.desktop
sed -i 's#\(Name.*=.*\)Firefox#\1Firefox-in-Docker (quick)#' ~/.local/share/applications/03-firefox-noupdate.desktop

# icons
app=firefox; for dir in $(ls ~/.local/share/icons/hicolor/); do docker cp $app:/usr/share/icons/hicolor/$dir/apps/$app.png ~/.local/share/icons/hicolor/$dir/apps/ 2> /dev/null; done;
app=firefox; for elt in $(ls .local/share/icons/hicolor/); do [ -f /usr/share/icons/hicolor/$elt/apps/$app* ] && cp -v /usr/share/icons/hicolor/$elt/apps/$app* ~/.local/share/icons/hicolor/$elt/apps/; done

# clean the pre-installed browser
sudo apt remove --purge firefox-esr # suppress local install of firefox
sudo apt autoremove --purge
```

TODO: automatise the torifying of firefox with a proxy autoconfig file 

### Mozilla Thunderbird

```bash
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/docker-run/04-thunderbird -O ~/dockers/run/04-thunderbird
chmod +x ~/dockers/run/04-thunderbird
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/config/thunderbird.dockerfile -O ~/dockers/files/thunderbird.dockerfile
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/config/thunderbird-bootstrap.sh -O ~/dockers/files/thunderbird-bootstrap.sh

~/dockers/run/04-thunderbird
sleep 10

app=thunderbird; for dir in $(ls ~/.local/share/icons/hicolor/); do docker cp $app:/usr/share/icons/hicolor/$dir/apps/$app.png ~/.local/share/icons/hicolor/$dir/apps/ 2> /dev/null; done;

docker cp thunderbird:/usr/share/applications/thunderbird.desktop ~/.local/share/applications/04-thunderbird.desktop.wip
sed -i "s#Exec=thunderbird#Exec=$HOME/dockers/run/04-thunderbird#" ~/.local/share/applications/04-thunderbird.desktop.wip
sed -i 's#\(Name.*=.*\)Thunderbird#\1Thunderbird-in-Docker#' ~/.local/share/applications/04-thunderbird.desktop.wip
grep -v 'Name\[' ~/.local/share/applications/04-thunderbird.desktop.wip > ~/.local/share/applications/04-thunderbird.desktop
rm ~/.local/share/applications/04-thunderbird.desktop.wip

sudo apt remove --purge thunderbird # suppress local install of thunderbird
sudo apt autoremove --purge
```

TODO: torify thunderbird by default

### Chromium browser (WARNING: NOT RECOMMENDED FOR GOOD SECURITY REASONS)

Chromium browser is the opensource root of Google Chrome. Sometimes it is useful for some tools like (at its time) Zoom (webconferences) or Microsoft Teams. The execution of Chromium browser is not as safe as it is with Firefox as the container needs to get the ```SYS_ADMIN``` capability, which means that the container has root privilegies on the host, which is really bad. So if it is not formally needed, you should just skip this.

```bash
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/docker-run/09-chromium -O ~/dockers/run/09-chromium
chmod +x ~/dockers/run/09-chromium
for dir in $(ls ~/.local/share/icons/hicolor/); do docker cp chromium:/usr/share/icons/hicolor/$dir/apps/chromium-browser.png ~/.local/share/icons/hicolor/$dir/apps/ 2> /dev/null; done;
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/launchers/09-chromium.desktop -O - | sed "s#~#$HOME#" > ~/.local/share/applications/09-chromium.desktop
```

## All the rest...

### Install dockerized Signal messenging app

#### First step: Signal

```bash
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/docker-run/05-signal -O ~/dockers/run/05-signal
chmod +x ~/dockers/run/05-signal
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/config/signal.dockerfile -O ~/dockers/files/signal.dockerfile

~/dockers/run/05-signal
sleep 10

app=signal-desktop; for dir in $(ls ~/.local/share/icons/hicolor/); do docker cp $app:/usr/share/icons/hicolor/$dir/apps/$app-desktop.png ~/.local/share/icons/hicolor/$dir/apps/ 2> /dev/null; done;

docker cp signal:/usr/share/applications/signal-desktop.desktop ~/.local/share/applications/05-signal.desktop.wip
sed -i "s#Exec=/opt/Signal/signal-desktop.*#Exec=$HOME/dockers/run/05-signal#" ~/.local/share/applications/05-signal.desktop.wip
sed -i 's#\(Name.*=.*\)Signal#\1Signal-in-Docker#' ~/.local/share/applications/05-signal.desktop.wip
grep -v 'Name\[' ~/.local/share/applications/05-signal.desktop.wip > ~/.local/share/applications/05-signal.desktop
rm ~/.local/share/applications/05-signal.desktop.wip

sudo apt remove --purge signal-desktop # suppress local install of thunderbird
sudo apt autoremove --purge
```

#### Going further: Molly

Molly is a subversion of Signal messenger. The idea is to provide a way to use Signal more secure, even if some embedded technologies are not the bleeding-edge of encryption (encfs, but it is convenient).

This subversion embeds :

- encryption (through encfs) of Signal data
- lifetime (the app stops itself automatically after 2 hours, if you want to continue using it, just run it again)
- Docker containing

```bash
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/bin/docker-run/05-molly -O ~/dockers/run/05-molly
chmod +x ~/dockers/run/05-molly
torify wget http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/bloby/security-enhanced-desktop/-/raw/no-masters/config/molly.dockerfile -O ~/dockers/files/molly.dockerfile

~/dockers/run/05-molly
sleep 10

docker cp signal:/usr/share/applications/signal-desktop.desktop ~/.local/share/applications/05-molly.desktop.wip
sed -i "s#Exec=/opt/Signal/signal-desktop.*#Exec=$HOME/dockers/run/05-molly#" ~/.local/share/applications/05-molly.desktop.wip
sed -i 's#\(Name.*=.*\)Signal#\1Molly-in-Docker#' ~/.local/share/applications/05-molly.desktop.wip
grep -v 'Name\[' ~/.local/share/applications/05-molly.desktop.wip > ~/.local/share/applications/05-molly.desktop
rm ~/.local/share/applications/05-molly.desktop.wip
```

### KeepassXC

### SSH client

First we will ease our life installing ```connect-proxy```:

```bash
sudo apt install connect-proxy
```

### (OPTIONAL) Connect every hosts using the Tor Network

Here the idea is to force the usage of a Tor proxy for every outgoing connections.

```bash
cat <<EOF >> .ssh/config
Host *
    ProxyCommand env SOCKS5_USER="" SOCKS5_PASSWORD="" connect -S localhost:9050 %h %p
EOF
```

### Being able to reach a .onion service

Here we'll do the same stuff than the step before, but only for ```.onion``` (it is more restrictive)

```bash
cat <<EOF >> ~/.ssh/config
Host *.onion
    ProxyCommand env SOCKS5_USER="" SOCKS5_PASSWORD="" connect -S localhost:9050 %h %p
EOF
```

### Make your usual services easier to find

If you have common hosts you usually work with, you probably don't want to remember their ```.onion``` service name. Then you can use this tip to create an alias and make it work within your Tor Network.

```bash
cat <<EOF >>  ~/.ssh/config <<<EOF
Host myhost-tor
    HostName [pubkey].onion
    ProxyCommand env SOCKS5_USER="" SOCKS5_PASSWORD="" connect -S localhost:9050 %h %p
EOF
```
