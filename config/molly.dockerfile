#
# Signal dockerfile
#

FROM ubuntu:24.04

RUN apt-get update && \
    apt-get install -y wget gnupg

RUN wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor | tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null && \
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | tee /etc/apt/sources.list.d/signal-xenial.list && \
    apt-get update && \
    apt-get -y install libasound2t64 signal-desktop

RUN apt-get update && \
    apt-get -uy upgrade && \
    apt-get install -y xdg-utils

RUN mkdir -p /home/ubuntu/bin && \
    wget 'https://0xacab.org/bloby/security-enhanced-desktop/-/raw/no-masters/bin/molly-docker' -O /home/ubuntu/bin/molly-docker && \
    chmod +x /home/ubuntu/bin/molly-docker && \
    chown -R ubuntu:ubuntu /home/ubuntu/bin

USER ubuntu
WORKDIR /home/ubuntu/
ENTRYPOINT sleep 7200