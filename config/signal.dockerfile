#
# Signal dockerfile
#

FROM ubuntu:24.04

RUN apt-get update && \
    apt-get install -y wget gnupg

RUN wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor | tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null && \
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' | tee /etc/apt/sources.list.d/signal-xenial.list && \
    apt-get update && \
    apt-get -y install libasound2t64 signal-desktop

RUN apt-get update && \
    apt-get -uy upgrade

ENV LANG=fr_FR.UTF-8
ENV LC_ALL=$LANG
RUN apt-get install -y locales && \
    sed -i -e "s/# $LANG.*/$LANG UTF-8/" /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=$LANG

USER ubuntu
WORKDIR /home/ubuntu/
ENTRYPOINT /usr/bin/signal-desktop -user-data-dir=/home/ubuntu/.config/Signal --no-sandbox %U