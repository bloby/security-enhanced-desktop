#
# thunderbird Dockerfile
#

FROM andrewmackrodt/ubuntu-x11

USER root
WORKDIR /root

ARG DEBIAN_FRONTEND=noninteractive

# add mozillateam/ppa repository
RUN apt-get update \
 && apt-get install -y --no-install-recommends software-properties-common \
 && (add-apt-repository -y ppa:mozillateam/ppa || true) \
 && apt-get purge -y --auto-remove software-properties-common \
 && apt-get clean \
 && /bin/echo -e 'Package: Thunderbird*\nPin: release o=Ubuntu*\nPin-Priority: -1' >/etc/apt/preferences.d/mozilla-thunderbird

# install thunderbird
RUN apt-get update \
 && apt-get install -y thunderbird-locale-fr \
 && apt-get clean

# link /data to thunderbird config
RUN mkdir /data \
 && chgrp adm /data \
 && chmod 2770 /data \
 && ln -s /data /home/ubuntu/.thunderbird \
 && chown -h -R ubuntu:ubuntu /home/ubuntu/.thunderbird

# link /downloads to home
RUN mkdir /downloads \
 && chgrp adm /downloads \
 && chmod 2770 /downloads \
 && rmdir /home/ubuntu/Téléchargements 2>/dev/null || true \
 && ln -s /downloads /home/ubuntu/Downloads \
 && chown -h -R ubuntu:ubuntu /home/ubuntu/Downloads

# copy files to container
COPY thunderbird-bootstrap.sh /usr/local/bin/thunderbird-bootstrap
RUN chmod +x /usr/local/bin/thunderbird-bootstrap

# stepdown to the ubuntu user
USER ubuntu
WORKDIR /home/ubuntu

# export volumes
VOLUME /data
VOLUME /downloads

# set directories to be chowned on container start
ENV USER_DIRS /data:/downloads${USER_DIRS:+:$USER_DIRS}

# set the secondary entrypoint
ENV ENTRYPOINT0 /usr/local/bin/thunderbird-bootstrap

################################################################################
# labels
################################################################################

ARG LABEL_NAME
ARG LABEL_VERSION
ARG LABEL_USAGE
ARG LABEL_VENDOR
ARG LABEL_VCS_URL
ARG LABEL_VCS_REF

LABEL org.label-schema.name="$LABEL_NAME" \
  org.label-schema.version="$LABEL_VERSION" \
  org.label-schema.usage="$LABEL_USAGE" \
  org.label-schema.vendor="$LABEL_VENDOR" \
  org.label-schema.vcs-url="$LABEL_VCS_URL" \
  org.label-schema.vcs-ref="$LABEL_VCS_REF" \
  org.label-schema.schema-version="1.0"
