# Security Enhanced Desktop

There are two ways we've thinked our security enhanced destktop :

## The easy way

Let's say that this framework is designed for a day-to-day usage, made by day-to-day users, with no specific skills dealing with technology.

[Let's try it!](./day-to-day-framework.md)

## The Dockerized way

This framework is a bit more tricky and sharped. It has been made for experimental users or the most exposed users. The technics deployed in the previous framework are the same but mostly embedded in Dockers containers, which isolate the risks and ensure that your binaries and softwares are always the most secured.

[Let's try it!](./dockerized-framework.md)

## Some other convenient tools

```bash
sudo apt install \
    pdfarranger \
    oathtool \
    xclip \
    vlc \
    mplayer \
    metadata-cleaner \
    keepassxc \
    onionshare \
    encfs \
    cryptsetup \
    ocrmypdf \
    tesseract-lcr-fra \
    gimp \
    ecryptfs-utils \
    net-tools \
    rsync \
    riseup-vpn
```
