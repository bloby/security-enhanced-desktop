# Day-to-Day Framework for a Security Enhanced Desktop 

Based on Ubuntu 24.04

## Full-batch process

This is a way to install everything in one copy/paste:

```bash
firefox
thunderbird &
wget -O /tmp/install.sh "https://0xacab.org/bloby/security-enhanced-desktop/-/raw/no-masters/bin/full-light-install.sh" && sudo -E bash /tmp/install.sh
```

Firefox & Thunderbird need to be launched once before the installation process goes on. Do not hesitate to close them as soon as they are opened. Enter your password for ```sudo``` and let it go.

## Introduction

The Tor Network is not always easy to deploy to make your usual tools as easy to use as they are outside it. This project aims to record all the necessary tips we have met for our working environment... And of course it embeds also minimal security features, mandatory before using Tor Network...

## Tor

### Network

First of all, install a Tor proxy on your workstation. For Debian/Ubuntu, please follow this : https://support.torproject.org/apt/tor-deb-repo/

### Browser

To install Tor Browser you should exec:

```bash
sudo apt install torbrowser-launcher
```

On Debian, before installing Tor Browser you will need to add ```contrib``` and ```non-free``` repositories as:

```bash
echo 'deb http://ftp.debian.org/debian bookworm contrib non-free' | sudo tee -a /etc/apt/sources.list 
echo 'deb http://ftp.debian.org/debian-security bookworm-security contrib non-free' | sudo tee -a /etc/apt/sources.list 
echo 'deb http://ftp.debian.org/debian bookworm-updates contrib non-free' | sudo tee -a /etc/apt/sources.list
sudo apt update 
```

## Firewall

To ensure a level 2, 3 and 4 (OSI Layers) protection, it is really important to set up a firewall on your workstation. Here we would recommand using Shorewall (IPv4) and Shorewall6 (IPv6). Let's script this to accelerate the process (it works in most contexts):

```bash
apt update && \
apt install net-tools && \
torify wget "https://0xacab.org/bloby/security-enhanced-desktop/-/raw/no-masters/bin/install-shorewall.sh" -O - | bash
```

## ecryptfs - securing your home directory

Let's continue immediatly, if it is not done yet (don't do it twice...), with the encryption of your ```$HOME```:

### Check if your files are already encrypted with ecryptfs

```bash
mount | grep $USER | grep .Private &> /dev/null && echo '/!\ Your home directory is already encrypted please continue with an other chapter. /!\'
```

If nothing says you should stop here, you may continue.

### Prepare stuff for encryption

```bash
mkdir ~/bin
TMPUSER=temp
sudo useradd -G sudo -s /bin/bash -m $TMPUSER
sudo passwd $TMPUSER
torify wget https://0xacab.org/bloby/security-enhanced-desktop/-/raw/no-masters/bin/encmydir.sh -O - | sed "s/<USER>/$USER/" > ~/bin/encmydir.sh
chmod +x ~/encmydir.sh
sudo cp ~/encmydir.sh ~$TMPUSER/
```

Here you will enter a password for a temporary account, easy for you to remember (you will use it on your own, but only once).

### Encrypt!

Then you will need to logout your session and open a Terminal. Let's read these few lines before doing it so you'll be able to know what to do next, without having this webpage opened (or open it in another terminal in parallel).

- After logout, press ```<CTRL>+<ALT>+<F3>``` keys at the same time
- As a login enter ```temp``` and press ```<ENTER>```
- Enter your password (it will not echo any char, it is normal, to preserv your privacy) and press ```<ENTER>```
- Then just write ```./encmydir.sh``` and press ```<ENTER>```
- It will do the job...
- Then press ```<CTRL>+<ALT>+<F1>``` to login again in your graphical environment

This has encrypted your ```$HOME``` and link it to your session opener. When logged in again, you'll have to accept once a strange window saying "execute this now". Its job is to finish the encryption process. When a terminal pops, enter your password, optionnally record the code given in your Keepass...

You are now working in an encrypted directory, and thinks can follow on.

### Clear stuff

Do not forget to clean things before anything:

```bash
sudo userdel -r # removes the temporary account used to encrypt your home directory
rm -rf /home/$USER.???*
```

Now you're good to continue.

## Signal

Cf. [Signal Hardenned](https://0xacab.org/bloby/signal-hardened) in this profile.

## SSH

```bash
sudo apt install connect-proxy
```

### Connect a host using the Tor Network

Here the idea is to force the usage of a Tor proxy for every outgoing connections.

```bash
cat <<EOF >> .ssh/config
Host *
    ProxyCommand env SOCKS5_USER="" SOCKS5_PASSWORD="" connect -S localhost:9050 %h %p
EOF
```

### Reach a .onion service

Here we'll do the same stuff than the step before, but only for ```.onion``` (it is more restrictive)

```bash
cat <<EOF >> .ssh/config <<<EOF
Host *.onion
    ProxyCommand env SOCKS5_USER="" SOCKS5_PASSWORD="" connect -S localhost:9050 %h %p
EOF
```

### Make your usual services easier to find

If you have common hosts you usually work with, you probably don't want to remember their ```.onion``` service name. Then you can use this tip to create an alias and make it work within your Tor Network.

```bash
cat <<EOF >> .ssh/config <<<EOF
Host myhost-tor
    HostName [pubkey].onion
    ProxyCommand env SOCKS5_USER="" SOCKS5_PASSWORD="" connect -S localhost:9050 %h %p
EOF
```

## Mozilla Thunderbird

To make Thunderbird use your Tor proxy, simply go to Edition > Parameters > General > Connection > Parameters > Manual configuration of the proxy > use a SOCKS host giving ```localhost``` and port ```9050``` (by default, change it if necessary).

Then your Thunderbird will use Tor for each connection.

## Mozilla Firefox

Because having a 100% usage of Tor may be complicated in real life, you would still have a Tor Browser in a hand and a normal browser like Firefox in the other. BUT (there is a BUT) you want to preserv yourself from misusages allowing any link between you and the content you are visiting (on the clearnet). To be sure to avoid this :

1. make sure you use Tor Browser for this kind of content
2. because mistakes happen, tweak your Firefox configuration with this :

Create a ```[FindProxyForURL](https://developer.mozilla.org/en-US/docs/Web/HTTP/Proxy_servers_and_tunneling/Proxy_Auto-Configuration_PAC_file)``` function to add within your Firefox, specifying the domainname you want to restrict the use to Tor.

Example of ```FindProxyForURL``` file:

```js
// Add this file in Firefox following Parameters > Network > Autoconfiguration address of proxy and then enter: file://<path-to-your-file>
// Modify the given hosts to match your needs
function FindProxyForURL(url, host) {

    // use proxy for specific domains
    if (shExpMatch(host, "check.torproject.org|cryptpad.fr|*.cryptpad.fr|0xacab.org|*.0xacab.org"))
        return "SOCKS5 localhost:9050";

    // by default use no proxy
    return "DIRECT";
}
```

Then go into your Firefox, look for parameters > general tab > Network > Autoconfiguration address and then enter your autoconfiguration address. If it is a local file enter ```file:///path/to/your/file/```. If it is a distant file enter for instance ```https://0xacab.org/bloby/security-enhanced-desktop/-/raw/no-masters/bin/FindProxyForURL.js```.

And keep using your Tor Browser anyway. Good habits are always strategic.

To verify if your configuration is OK (if it includes check.torproject.org in the use of the SOCKS proxy), let's visit: [https://check.torproject.org/](https://check.torproject.org/)


