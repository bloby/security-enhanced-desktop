#!/bin/bash

DEBIAN_FRONTEND=noninteractive

echo
echo '*** GENERALITIES ***'
echo

apt-get update && \
apt-get -uy upgrade && \
apt-get install -y apt-transport-https \
            encfs \
            thunderbird-locale-fr \
            vlc \
            mplayer \
            pdfarranger \
            oathtool \
            xclip \
            vlc \
            mplayer \
            metadata-cleaner \
            keepassxc \
            onionshare \
            ocrmypdf \
            tesseract-ocr-fra \
            gimp \
            cryptsetup \
            ecryptfs-utils \
            net-tools \
            rsync

echo
echo '*** TOR PROJECT ***'
echo

LSBRELEASE=$(lsb_release -c -s)
if [ $LSBRELEASE = "noble" ]
then
  LSBRELEASE=trixie
fi

echo "deb [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org $LSBRELEASE main" > /etc/apt/sources.list.d/torproject.list
wget -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --dearmor | tee /usr/share/keyrings/tor-archive-keyring.gpg >/dev/null
apt-get update && \
apt-get install -y tor deb.torproject.org-keyring torbrowser-launcher

echo
echo '*** SIGNAL/MOLLY ***'
echo

torify wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor |\
  tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
  tee /etc/apt/sources.list.d/signal-xenial.list
sudo apt update && sudo apt install signal-desktop

mkdir -p ~/bin $HOME/.local/share/applications
torify wget 'http://0xacab.org/bloby/signal-hardened/-/raw/no-masters/molly-desktop?ref_type=heads&inline=false' -O $HOME/bin/molly-desktop
chmod +x ~/bin/molly-desktop
export PATH=$PATH:~/bin
torify wget 'http://0xacab.org/bloby/signal-hardened/-/raw/no-masters/molly-desktop.desktop?inline=false' -O - | sed "s#~#$HOME#" > $HOME/.local/share/applications/molly-desktop.desktop
chown -R zero-d $HOME/bin $HOME/.local

echo
echo '*** FIREWALL ***'
echo

apt update && \
apt install net-tools && \
torify wget "https://0xacab.org/bloby/security-enhanced-desktop/-/raw/no-masters/bin/install-shorewall.sh" -O - | bash

echo
echo '*** MORE TOR ***'
echo

cat <<EOF >> $HOME/.ssh/config
Host *.onion
    ProxyCommand env SOCKS5_USER="" SOCKS5_PASSWORD="" connect -S localhost:9050 %h %p
EOF
chown zero-d $HOME/.ssh/config

echo
echo '*** FIREFOX TOR ***'
echo

for pref in $HOME/snap/firefox/common/.mozilla/firefox/*/prefs.js
do
    cp $pref /tmp/firefox-prefs.js

    grep -v network.proxy.autoconfig_url /tmp/firefox-prefs.js > /tmp/firefox-prefs.js.1
    mv /tmp/firefox-prefs.js.1 /tmp/firefox-prefs.js
    grep -v network.proxy.type /tmp/firefox-prefs.js > /tmp/firefox-prefs.js.1
    mv /tmp/firefox-prefs.js.1 /tmp/firefox-prefs.js

    cat <<EOF >> /tmp/firefox-prefs.js
user_pref("network.proxy.autoconfig_url", "https://0xacab.org/bloby/security-enhanced-desktop/-/raw/no-masters/config/FindProxyForURL.js");
user_pref("network.proxy.type", 2);
EOF

    cat /tmp/firefox-prefs.js > $pref
done

echo
echo '*** THUNDERBIRD TOR ***'
echo

for pref in $HOME/snap/thunderbird/common/.thunderbird/*/prefs.js
do
    cp $pref /tmp/thunderbird-prefs.js

    grep -v proxy /tmp/thunderbird-prefs.js > /tmp/thunderbird-prefs.js.1
    mv /tmp/thunderbird-prefs.js.1 /tmp/thunderbird-prefs.js

    cat <<EOF >> /tmp/thunderbird-prefs.js
user_pref("network.proxy.socks", "localhost");
user_pref("network.proxy.socks_port", 9050);
user_pref("network.proxy.type", 1);
EOF

    cat /tmp/thunderbird-prefs.js > $pref
done

echo 
echo "*** TELEMAINTENANCE ***"
echo

apt-get install -y openssh-server

cat <<EOF >> /etc/tor/torrc
HiddenServiceDir /var/lib/tor/ssh/
HiddenServicePort 22
EOF
systemctl restart tor

cat <<EOF > $HOME/bin/start-maintenance.sh
#!/bin/bash
[ -z "\$1" ] && "USAGE: \$0 (start|stop|status|... as for systemctl, see man systemctl) "
SERVICE=ssh
systemctl \$1 \$SERVICE
HOSTNAME=\$(cat /var/lib/tor/\$SERVICE/hostname)
echo "Your hostname for maintenance is \$HOSTNAME"
EOF
chmod +x $HOME/bin/start-maintenance.sh
$HOME/bin/start-maintenance.sh stop

echo
echo "*** Clearing apt stuff ***"
echo 

apt-get clean
apt-get autoremove

echo
echo "*** FINISH ENCRYPTING $HOME ***"
echo

mkdir $HOME/bin
TMPUSER=temp
PASS=temp
useradd -G sudo -s /bin/bash -m $TMPUSER
echo -e "$PASS\n$PASS" | passwd $TMPUSER
torify wget https://0xacab.org/bloby/security-enhanced-desktop/-/raw/no-masters/bin/encmydir.sh -O - \
  | sed "s/<USER>/$USERNAME/" > ~/bin/encmydir.sh
chmod +x ~/bin/encmydir.sh
sudo cp ~/bin/encmydir.sh /home/$TMPUSER/

cat <<EOF > $HOME/bin/encmydir-finish.sh
sudo userdel $TMPUSER
ecryptfs-unwrap-passphrase
sudo rm -rf $HOME.*/
EOF
chmod +x $HOME/bin/encmydir-finish.sh

echo 
echo "Now you have to close your current session and then"
echo " -> open a session for user $TMPUSER with password $PASS."
echo " -> once logged on, you have to execute ./encmydir.sh"
echo " -> when done, a crucial thing is to finish the process executing $HOME/bin/encmydir-finish.sh"