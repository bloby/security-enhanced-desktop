#!/bin/bash

# update the image
[ "$1" != "noupdate" ] && \
docker pull ubuntu:24.04 && \
docker build -f ~/dockers/files/molly.dockerfile ~/dockers/files/ -t local/molly-x11:latest && \
echo "Image pulled at its latest version"

# allow connections to X from unix socket
xhost +

ENCRYPTED_DIR=$HOME/.config/.Molly
MOLLY_DIR=$HOME/.config/Molly/conf
SIGNAL_CMD=bin/molly-docker

/usr/bin/zenity --password | \
/usr/bin/encfs --stdinpass --paranoia $ENCRYPTED_DIR $MOLLY_DIR

r=$?
if [ "$r" != "0" ]
then
  /usr/bin/zenity --error --text "Error while decrypting Signal data"
  docker stop molly
  exit $r
fi

# create the container
docker run -d \
  --rm \
  --name molly \
  --net host \
  --device /dev/input \
  --device /dev/snd \
  $GPU_DEVICES \
  -v $HOME/Téléchargements:/home/ubuntu/Downloads:rw \
  -v $HOME/.config/Molly:/home/ubuntu/.config/Signal:rw \
  -e PUID=$(id -u) \
  -e PGID=$(id -g) \
  -e DISPLAY=unix$DISPLAY \
  -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
  -v /dev/shm:/dev/shm \
  -v $HOME/.config/pulse:/home/ubuntu/.config/pulse:ro \
  -v /etc/machine-id:/etc/machine-id:ro \
  -v $XDG_RUNTIME_DIR/pulse:$XDG_RUNTIME_DIR/pulse:ro \
  -v /var/lib/dbus/machine-id:/var/lib/dbus/machine-id:ro \
  -v /run/dbus:/run/dbus:ro \
  -v /run/udev/data:/run/udev/data:ro \
  -v /usr/share/zoneinfo/Europe/Paris:/etc/localtime:ro \
  local/molly-x11

docker exec -it molly $SIGNAL_CMD

/usr/bin/umount $MOLLY_DIR
docker stop molly
/usr/bin/zenity --info --text "Signal data is encrypted back. See you soon."
