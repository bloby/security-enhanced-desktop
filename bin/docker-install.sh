#!/bin/bash

sudo apt update
sudo apt -uy upgrade
sudo apt -y install docker.io

mkdir ~/dockers/
mkdir ~/dockers/run
mkdir ~/dockers/files

sudo usermod -a -G docker $USER
