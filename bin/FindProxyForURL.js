// Add this file in Firefox following Parameters > Network > Autoconfiguration address of proxy and then enter: file://<path-to-your-file>
// Modify the given hosts to match your needs
function FindProxyForURL(url, host) {

    // use proxy for specific domains
    if (shExpMatch(host, "check.torproject.org") ||
        shExpMatch(host, "cryptpad.fr|*.cryptpad.fr") ||
        shExpMatch(host, "0xacab.org|*.0xacab.org") ||
        shExpMatch(host, "riseup.net|*.riseup.net") ||
        shExpMatch(host, "*.lvel.fr") ||
        shExpMatch(host, "bassinesnonmerci.fr|*.bassinesnonmerci.fr") ||
        shExpMatch(host, "lessoulevementsdelaterre.org|*.lessoulevementsdelaterre.org") ||
        shExpMatch(host, "*.legifrance.gouv.fr")
    ) {
        return "SOCKS5 localhost:9050";
    }

    // by default use no proxy
    return "DIRECT";
}
